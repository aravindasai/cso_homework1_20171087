#!/bin/bash

while ( true )
do
	let a=$1+7
	let b=$1
	top -b -o +%MEM | head -n $a | tail -n $b | cut -d ":" -f 2 | cut -d " " -f 2,3

	echo

	sleep $2
done 
