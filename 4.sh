   #!/bin/bash

   tmux new-session -s $1 -d
   tmux split-window -v -d -t $1:0 " sudo tcpdump port 80 or port 443"
   tmux split-window -h -d -t $1:0 " watch -n 1 date"
   tmux split-window -v -d -t $1:0 "htop"
   tmux kill-pane
   tmux select-pane -t:.2
   tmux attach
